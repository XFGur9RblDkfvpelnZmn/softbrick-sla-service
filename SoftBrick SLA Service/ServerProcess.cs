﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceProcess;
using Algemeen;

namespace SoftBrick_SLA_Service
{
    class ServerProcess : ServiceController
    {
        public ServerProcess():base("SoftBrick_server_service")
        {
            
            
        }
        public bool IsRunning()
        {
            if (this.Status == ServiceControllerStatus.Running)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void StartService()
        {
            
            try
            {
                int iTimeOut = 30000;
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(iTimeOut);
                try
                {
                    this.Stop();
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
                this.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(iTimeOut - (millisec2 - millisec1));

                this.Start();
                this.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch(Exception ex)
            {
                Log.Exception(ex);
            }
        }
    }
}
