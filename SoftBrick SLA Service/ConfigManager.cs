﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SoftBrick_SLA_Service
{
    public  class ConfigManager
    {
        public static bool autoStartService
        {
            get
            {
                return Properties.Settings.Default.autoStartService;
            }
            set
            {
                Properties.Settings.Default.autoStartService = value;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
            }

        }
        public static string emailWhenDown
        {
            get
            {
                return Properties.Settings.Default.emailWhenDown;
            }
            set
            {
                Properties.Settings.Default.emailWhenDown = value;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
            }

        }
        public static string smtpServer
        {
            get
            {
                return Properties.Settings.Default.smtpServer;
            }
            set
            {
                Properties.Settings.Default.smtpServer = value;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
            }

        }
        public static int checkServiceEveryXMs
        {
            get
            {
                return Properties.Settings.Default.checkServiceEveryXMs;
            }
            set
            {
                Properties.Settings.Default.checkServiceEveryXMs = value;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
            }

        }
    }
}
