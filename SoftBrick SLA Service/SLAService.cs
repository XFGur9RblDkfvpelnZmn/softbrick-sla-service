﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Timers;

namespace SoftBrick_SLA_Service
{
    public partial class SLAService : ServiceBase
    {
        Timer tCheckServiceTimer;
        bool bMailAlreadySent = false;
        public SLAService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (Properties.Settings.Default.checkServiceEveryXMs > 0)
            {
                tCheckServiceTimer = new Timer();
                tCheckServiceTimer.Elapsed += new ElapsedEventHandler(checkServiceMethod);
                tCheckServiceTimer.Interval = Properties.Settings.Default.checkServiceEveryXMs;
                tCheckServiceTimer.Enabled = true;
            }
        }

        
        
        protected override void OnStop()
        {
        }

        protected void checkServiceMethod(object source, ElapsedEventArgs e)
        {
            ServerProcess spSoftbrickService = new ServerProcess();
            if (!spSoftbrickService.IsRunning())
            {
                
                if (Properties.Settings.Default.autoStartService == true)
                {
                    spSoftbrickService.StartService();
                    if (Properties.Settings.Default.emailWhenDown.Length > 0 && bMailAlreadySent == false)
                    {
                        sendMail("SoftBrick Service Down", "Beste\n\r\n\rDe softbrick service is gestopt op uw productieserver en is automatisch herstart. Verder acties zijn niet vereist.\n\r\n\rMet vriendelijke groeten\n\rReBus SLA Controle Service");
                    }
                }
                else if (Properties.Settings.Default.emailWhenDown.Length > 0 && bMailAlreadySent == false)
                {
                    sendMail("SoftBrick Service Down", "Beste\n\r\n\rDe softbrick service is gestopt op uw productieserver.\n\rHerstart de service of contacteer ReBus.\n\r\n\rMet vriendelijke groeten\n\rReBus SLA Controle Service");
                }
            } 
            else
            {
                bool bIsRunning = false;
                Process[] processlist = Process.GetProcessesByName("tcserver"); ;
                foreach(Process theprocess in processlist){
                    bIsRunning = true;
                    bMailAlreadySent = false;
                }
                if (bIsRunning == false)
                {
                    if (Properties.Settings.Default.autoStartService == true)
                    {
                        spSoftbrickService.StartService();
                        if (Properties.Settings.Default.emailWhenDown.Length > 0 && bMailAlreadySent == false)
                        {
                            sendMail("SoftBrick Proces Down", "Beste\n\r\n\rHet softbrick service proces is gestopt op uw productieserver en is automatisch herstart. Verder acties zijn niet vereist.\n\r\n\rMet vriendelijke groeten\n\rReBus SLA Controle Service");
                        }
                    }
                    else if (Properties.Settings.Default.emailWhenDown.Length > 0 && bMailAlreadySent == false)
                    {
                        sendMail("SoftBrick Proces Down", "Beste\n\r\n\rHet softbrick service proces is gestopt op uw productieserver.\n\rHerstart de service of contacteer ReBus.\n\r\n\rMet vriendelijke groeten\n\rReBus SLA Controle Service");
                    }
                }
            }
        }

        protected void sendMail(string sSubject, string sMessage)
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            foreach (string s in Properties.Settings.Default.emailWhenDown.Split(';'))
            {
                message.To.Add(s);
            }
            message.Subject = sSubject;
            message.From = new System.Net.Mail.MailAddress("support@rebus-it.nl");
            message.Body = sMessage;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Properties.Settings.Default.smtpServer);
            smtp.Send(message);
            bMailAlreadySent = true;
        }

        
    }
}
