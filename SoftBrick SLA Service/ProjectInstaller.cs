﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using System.IO;

namespace SoftBrick_SLA_Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        
        private void serviceInstaller1_Committed(object sender, InstallEventArgs e)
        {
            new ServiceController(serviceInstaller1.ServiceName).Start();
            
            //Algemeen.Proces p = new Algemeen.Proces(new FileInfo(this.Context.Parameters["AssemblyPath"]).Directory+ "\\ConfigManager.exe");
            
            
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
