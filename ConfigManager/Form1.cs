﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ConfigManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SoftBrick_SLA_Service.ConfigManager.autoStartService = cbAutoStart.Checked;
            SoftBrick_SLA_Service.ConfigManager.checkServiceEveryXMs = Convert.ToInt32(txtRefresh.Text);
            SoftBrick_SLA_Service.ConfigManager.emailWhenDown = txtEmail.Text;
            SoftBrick_SLA_Service.ConfigManager.smtpServer = txtSmtp.Text;
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbAutoStart.Checked = SoftBrick_SLA_Service.ConfigManager.autoStartService;
            txtRefresh.Text = SoftBrick_SLA_Service.ConfigManager.checkServiceEveryXMs.ToString();
            txtEmail.Text = SoftBrick_SLA_Service.ConfigManager.emailWhenDown;
            txtSmtp.Text = SoftBrick_SLA_Service.ConfigManager.smtpServer;
        }
    }
}
